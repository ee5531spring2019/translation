#!/usr/bin/env python
   
import sys
import rospy
import json
import requests
from dictionary.srv import *
   
def translate_word_client(word):
    rospy.wait_for_service('translate_word')
    try:
        translate_word = rospy.ServiceProxy('translate_word', TranslateWord)
        resp1 = translate_word(word)
        return resp1.translations
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
   
def usage():
    return "%s [word]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 2:
        word = sys.argv[1]
    else:
        print usage()
        sys.exit(1)
    print "Translating... %s"%(word)
    print "%s"%(translate_word_client(word))
