#! /usr/bin/env python

from dictionary.srv import *
import rospy
import json
import requests

api_id = '2e8fd4d8'
api_key = 'e847e7f75dee2a5d44915a1013d6e72d'
    
def handle_translate_word(req):
    main_api ='https://od-api.oxforddictionaries.com:443/api/v1/entries/en/'
    url = main_api + req.word + '/translations=de'
    r = requests.get(url, headers = {'app_id':api_id, 'app_key':api_key})
    json_data = r.json()
    print json_data['results'][0]['lexicalEntries'][0]['entries'][0]['senses'][0]['translations'][0]['text']
    return TranslateWordResponse(req.word)

def translate_word_server() :
    rospy.init_node('translate_word_server') 
    s = rospy.Service('translate_word', TranslateWord, handle_translate_word)
    print "Please input your word to be translated"
    rospy.spin()

if  __name__ =="__main__":
    translate_word_server ()
